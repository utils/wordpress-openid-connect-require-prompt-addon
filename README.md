# OpenID Connect Require Prompt Must-Use Plugin Add-on

Add-on to be used with [OpenID Connect Generic Plugin](https://github.com/daggerhart/openid-connect-generic).

Purpose of this add-on is to force the OIDC IdP ask for consent to hand over user's information every time user logs in. More about plugin installation
and configuration can be found [here](https://www.mojeid.cz/dokumentace/html/ImplementacePodporyMojeid/OpenidConnect/KnihovnyModuly/wordpress.html).

## Add-on installation

1. Download the main plugin file, `oidc-require-prompt-addon.php` to your local machine.
2. Upload the main plugin file to the `wp-content/mu-plugins/` directory on your site. If the `mu-plugins` directory doesn't exist, create it.
3. Navigate to the OpenID Connect Generic plugin settings page.
4. Tick the `Require prompt` checkbox and save.
